#include "raytracer.h"
#include <cmath>

/*-<==>-----------------------------------------------------------------
/
/---------------------------------------------------------------------*/
CCamera::CCamera()
{
    // Initialize with some default parameters
    setRenderParameters(320, 240, 60.0f);
    lookAt(VECTOR(0, 0, 0), VECTOR(0, 0, 1));
}

CCamera::~CCamera()
{
}

/*-<==>-----------------------------------------------------------------
/ Save render parameters for later use
/ fov is in degrees
/---------------------------------------------------------------------*/
void CCamera::setRenderParameters(int axres, int ayres, SCALAR afov_in_deg)
{
    // Pendiente de implementar correctamente
    // ...
    // Compute view_d from afov_in_deg
    // ...
    xres = axres;
    yres = ayres;
    fov = afov_in_deg * M_PI / 180.;
    viewd = (yres / 2.) / tan(fov / 2.);
}

/*-<==>-----------------------------------------------------------------
/ Save the new camera position and target point.
/ Define the axis of the camera (front, up, left) in world coordinates
/ based on the current values of the vectors target & loc
/---------------------------------------------------------------------*/
void CCamera::lookAt(const VECTOR &src_point, const VECTOR &dst_point)
{
    loc = src_point;
    target = dst_point;
    // Pendiente de implementar correctamente
    // ...
    front = target - loc;
    front.normalize();
    left = VECTOR(0, 1, 0).cross(front);
    left.normalize();
    up = front.cross(left);
}

/*-<==>-----------------------------------------------------------------
/ return a line which starts on camera position and goes through the pixel
/ (x,y) from the screen
/---------------------------------------------------------------------*/
CLine CCamera::getLineAt(SCALAR x, SCALAR y) const
{
    VECTOR v =
        viewd * front + up * (yres / 2 - y) + left * (xres / 2 - x);
    v.normalize();
    return CLine(loc, v);
}
