#include <assert.h>
#include "raytracer.h"
#include "image.h"
#include "line.h"
#include <iostream>
#include <cstdlib>
#include "utils.h"
#include <random>
#include <chrono>
#include <cmath>
#include "logger.h"
#include <vector>

/*-<==>-----------------------------------------------------------------
/
/----------------------------------------------------------------------*/
CRayTracer::CRayTracer()
    : max_recursion_level(10)
    , background_color(COLOR(0, 0, 0))
    , ambient_color(COLOR(0, 0, 0))
    , random_generator(std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()))
    , logger()
{
}

/*-<==>-----------------------------------------------------------------
/ Create an image, and for each pixel in the screen create a line
/ and retrieve which color is seen through this line
/ Save the image to a file
/----------------------------------------------------------------------*/
void CRayTracer::render(const char *filename)
{
    CLine line;
    CBitmap img(camera.getXRes(), camera.getYRes(), 24);
    std::normal_distribution<SCALAR> randdist(0, 0.1);
    SCALAR d = 1. / (SCALAR) ANTIALIAS;
    std::vector<Logger> loggers(camera.getXRes(), Logger());
    #pragma omp parallel for default(shared) private(line)
    for(int x = 0; x < camera.getXRes(); x++)
        for(int y = 0; y < camera.getYRes(); y++) {
            COLOR pixel(0, 0, 0);
            for(int j=0; j<ANTIALIAS; j++)
                for(int i=0; i<ANTIALIAS; i++) {
                    SCALAR xc = (SCALAR)x + i*d + d/2 - .5;
                    SCALAR yc = (SCALAR)y + j*d + d/2 - .5;
                    line = camera.getLineAt(xc, yc);
                    trace(line, loggers[x]);
                    loggers[x].traceCameraRay();
                    pixel += line.color;
                }
            img.setPixel(x, y, clamp_color(pixel / ((SCALAR)ANTIALIAS*(SCALAR)ANTIALIAS)));
        }
    for(int x = 0; x < camera.getXRes(); x++)
        logger += loggers[x];
    img.saveTGA(filename);
}

/*-<==>-----------------------------------------------------------------
/ Find which object and at which 't' the line hits and object
/ from the scene.
/ Returns true if the object hits some object
/----------------------------------------------------------------------*/
bool CRayTracer::intersects(CLine &line, Logger &logger)
{
    SCALAR t;
    LRTObjects::iterator i = objects.begin();
    while(i != objects.end()) {
        CRTObject *obj = *i++;
        bool xsec_test = obj->hits(line, t);
        logger.countIntersectionTest(xsec_test);
        if(xsec_test) {
            if(t < line.t || line.t < 0) {
                line.t = t;
                line.obj = obj;
            }
        }
    }
    bool result = line.t >= 0;
    return result;
}

/*-<==>-----------------------------------------------------------------
/ Returns in line.color the color captured by the line.
/----------------------------------------------------------------------*/
void CRayTracer::trace(CLine &line, Logger &logger)
{
    background(line);
    if(line.getLevel() > max_recursion_level || !intersects(line, logger)) {
        logger.countLastRecursionRay();
        return;
    }

    line.color = ambient_color;

    VECTOR pint = line.getIntersection();
    CRTObject *o = line.obj;
    VECTOR normal = o->getNormal(pint);
    CMaterial *m = o->getMaterial();
    VECTOR incident = line.dir;
    SCALAR cosI = -normal.dot(incident);
    CLine reflected(pint, 2 * cosI * normal + incident, line.getLevel()+1);

    SCALAR trans = m->getTranslucency(pint);
    if(trans > EPSILON) {
	SCALAR n1, n2, n;
	if(cosI > 0) {
	    n1 = 1;
	    n2 = m->getIOR(pint);
	} else {
	    n1 = m->getIOR(pint);
	    n2 = 1;
	}
	n = n1 / n2;

	SCALAR reflectance;
	cosI = fabs(cosI);
	SCALAR sinT2 = n * n * (1 - cosI * cosI);
	SCALAR cosT = sqrt(1 - sinT2);
	if(sinT2 > 1)
	    reflectance = 1;
	else {
	    SCALAR rOrth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
	    SCALAR rPar  = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);
	    reflectance = (rOrth * rOrth + rPar * rPar) / 2;
	}
	SCALAR transparence = 1 - reflectance;
        bool doReflections = true;
	if(transparence > EPSILON) {
	    CLine refracted(pint, n * incident + (n * cosI - cosT) * (n2 > n1 ? normal : -normal), line.getLevel()+1);
	    trace(refracted, logger);
            logger.traceRefractionRay();
	    line.color += refracted.color * transparence * trans;
            // Discard partial internal reflections
            if(n1 > n2) doReflections = false;
	}

	if(doReflections && reflectance > EPSILON) {
	    trace(reflected, logger);
            logger.traceReflectionRay();
	    line.color += reflected.color * reflectance * trans;
	}
    }

    LLights::iterator light = lights.begin();
    for(; light != lights.end(); light++) {
        VECTOR l = (*light)->getLocation() - pint;
        CLine toLight(pint, l);
	bool intersectToLight = intersects(toLight, logger);
        logger.traceShadowRay();
        if(!intersectToLight || (toLight.t > l.length())) {
	    l = toLight.dir;
            SCALAR dot = fabs(l.dot(normal));
            SCALAR att = (*light)->getAttenuation();
            SCALAR len2 = l.length_squared();
            COLOR col = (*light)->getColor() / (1 + att * sqrt(len2) + att * len2);
            if(dot > EPSILON)
                line.color += dot * col.filter(m->getDiffuseColor(pint));
            dot = l.dot(reflected.dir);
            if(dot > EPSILON) {
		SCALAR dotpow = dot;
		// Phong coefficient always assumed to be 2^6 = 64
		for(int i=0; i<6; i++)
		    dotpow *= dotpow;
                line.color += dotpow * m->getSpecularity(pint) * col;
	    }
        }
    }
}

/*-<==>-----------------------------------------------------------------
/ Default background
/----------------------------------------------------------------------*/
void CRayTracer::background(CLine &line)
{
    line.color = background_color;
}
