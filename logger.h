#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <ostream>

typedef long long Counter;

class Logger
{
    Counter last_recursion_rays;
    Counter camera_rays;
    Counter shadow_rays;
    Counter refraction_rays;
    Counter reflection_rays;
    Counter neg_intersect_tests;
    Counter pos_intersect_tests;

    int n_primitives;
    int n_lights;
    int n_pixels;
    int n_antialias;

    void generate_report(std::ostream &out);
public:
    Logger();

    void report(const char *filename);
    void countLastRecursionRay();
    void traceCameraRay();
    void traceShadowRay();
    void traceRefractionRay();
    void traceReflectionRay();
    void countIntersectionTest(bool result);

    void operator+=(Logger other);

    void setNPrimitives(int n);
    void setNLights(int n);
    void setResolution(int xres, int yres, int antialias);
};

#endif //_LOGGER_H_
