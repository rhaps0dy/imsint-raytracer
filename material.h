#ifndef INC_MATERIAL_H_
#define INC_MATERIAL_H_

#pragma warning(disable : 4786)

/*-<==>-----------------------------------------------------------------
/ Generic Material definition
/ Some materials will not use the position to define the diffuse or
/ reflectance values
/----------------------------------------------------------------------*/
class CMaterial
{
  public:
    CMaterial()
    {
    }
    virtual ~CMaterial()
    {
    }
    virtual COLOR getDiffuseColor(const VECTOR &loc) const { return COLOR(0,0,0); }
    virtual SCALAR getSpecularity(const VECTOR &loc) const { return 0; }
    virtual SCALAR getTranslucency(const VECTOR &loc) const { return 0; }
    // IOR == 0 means total reflection always
    virtual SCALAR getIOR(const VECTOR &loc) const { return 0; }
};

/*-<==>-----------------------------------------------------------------
/ A map, key is a std::string and the value is a material pointer
/----------------------------------------------------------------------*/
#include <map>
#include <string>
typedef std::map<std::string, CMaterial *> MMaterials;

/*-<==>-----------------------------------------------------------------
/ Solid color material
/----------------------------------------------------------------------*/
class CSolidMaterial : public CMaterial
{
    COLOR diffuse_color;
    SCALAR specularity;
    SCALAR translucency;
    SCALAR ind_or;

  public:
    CSolidMaterial(const COLOR diff, SCALAR spec, SCALAR trans = 0, SCALAR ior = 0);
    COLOR getDiffuseColor(const VECTOR &loc) const;
    SCALAR getSpecularity(const VECTOR &loc) const;
    SCALAR getTranslucency(const VECTOR &loc) const;
    SCALAR getIOR(const VECTOR &loc) const;
};

class CCheckerboardMaterial : public CMaterial
{
    CMaterial *_even;
    CMaterial *_odd;
    SCALAR _grid_size;

    const CMaterial *matAtLoc(const VECTOR &loc) const;

public:
    CCheckerboardMaterial(CMaterial *even, CMaterial *odd, SCALAR grid_size);
    COLOR getDiffuseColor(const VECTOR &loc) const;
    SCALAR getSpecularity(const VECTOR &loc) const;
    SCALAR getTranslucency(const VECTOR &loc) const;
    SCALAR getIOR(const VECTOR &loc) const;
};

#endif // INC_MATERIAL_H_
