#include "cilinder.h"
#include "utils.h"
#include <set>

/*-<==>-----------------------------------------------------------------
/ Constructor
/----------------------------------------------------------------------*/
CCilinder::CCilinder(SCALAR radius, SCALAR aheight)
    : radius2(radius*radius)
    , height(aheight)
{
}

static void
insertIfInCircle(const CLine &line, SCALAR t, const VECTOR &loc, SCALAR radius2, std::set<SCALAR> &ts)
{
    VECTOR pint = line.getIntersection(t);
    SCALAR pintx = pint.x - loc.x;
    SCALAR pintz = pint.z - loc.z;
    if(t > EPSILON && pintx*pintx + pintz*pintz <= radius2)
	ts.insert(t);
}

static void
insertIfWithinHeight(const CLine &line, SCALAR t, const VECTOR &loc, SCALAR height, std::set<SCALAR> &ts)
{
    SCALAR y = line.getIntersection(t).y;
    if(t > EPSILON && loc.y < y && y < (loc.y + height))
	ts.insert(t);
}

bool CCilinder::hits(const CLine &line, SCALAR &t_hit)
{
    SCALAR t, a, b, c, disc;
    std::set<SCALAR> ts;
    VECTOR spos = line.loc - loc;

    a = line.dir.x*line.dir.x + line.dir.z*line.dir.z;
    b = 2*(line.dir.x*spos.x + line.dir.z*spos.z);
    c = spos.x*spos.x + spos.z*spos.z - radius2;
    disc = b*b - 4*a*c;
    if(disc > 0) {
	disc = sqrt(disc);
	insertIfWithinHeight(line, (-b - disc)/(2*a), loc, height, ts);
	insertIfWithinHeight(line, (-b + disc)/(2*a), loc, height, ts);
    }
    insertIfInCircle(line, -spos.y / line.dir.y, loc, radius2, ts);
    insertIfInCircle(line, (-spos.y + height) / line.dir.y, loc, radius2, ts);

    if(ts.begin() == ts.end())
	return false;
    t_hit = (*ts.begin());
    return true;
}

VECTOR CCilinder::getNormal(const VECTOR &hit_loc)
{
    SCALAR hit_height = hit_loc.y - loc.y;
    if(hit_height < EPSILON) {
	return VECTOR(0, -1, 0);
    } else if(hit_height > height - EPSILON) {
	return VECTOR(0, 1, 0);
    }
    VECTOR norm = hit_loc - loc;
    norm.y = 0;
    norm.normalize();
    return norm;
}
