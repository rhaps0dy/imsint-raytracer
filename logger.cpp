#include "logger.h"

#include <iostream>
#include <iomanip>
#include <fstream>

Logger::Logger()
    : last_recursion_rays(0), camera_rays(0), shadow_rays(0)
    , refraction_rays(0), reflection_rays(0)
    , neg_intersect_tests(0), pos_intersect_tests(0)
    , n_primitives(0), n_lights(0), n_pixels(0)
{
}

static double
coeff(long long a, long long b)
{
    return ((double)a) / ((double)b);
}

void Logger::generate_report(std::ostream &out)
{
    out << "Number of primitives: " << n_primitives << std::endl;
    out << "Number of lights: " << n_lights << std::endl;
    out << "Number of pixels: " << n_pixels << std::endl << std::endl;

    Counter total = camera_rays + shadow_rays + refraction_rays + reflection_rays;
    out << "Total number of rays (with recursive immediate-return calls): "
        << total << std::endl;
    out << " + Number of rays per pixel: "
        << coeff(total, n_pixels) << std::endl;
    out << " + Number of rays per primitive: "
        << coeff(total, n_primitives) << std::endl;
    out << " + Number of rays per light: "
        << coeff(total, n_lights) << std::endl;
    out << std::endl;

    total -= last_recursion_rays;
    out << "Total number of rays (without recursive immediate-return calls): "
        << total << std::endl;
    out << " + Number of rays per pixel: "
        << coeff(total, n_pixels) << std::endl;
    out << " + Number of rays per primitive: "
        << coeff(total, n_primitives) << std::endl;
    out << " + Number of rays per light: "
        << coeff(total, n_lights) << std::endl;
    out << std::endl;

    out << "Rays shot from camera: " << camera_rays << std::endl;
    out << " + Number of rays per pixel: "
        << coeff(camera_rays, n_pixels) << std::endl;
    out << "We should be shooting " << n_antialias
        << " rays per pixel for anti-aliasing" << std::endl;
    out << std::endl;

    out << "Rays to calculate shadowing: " << shadow_rays << std::endl;
    out << " + Number of rays per light: "
        << coeff(shadow_rays, n_lights) << std::endl;
    out << std::endl;

    out << "Rays to calculate reflection: " << reflection_rays << std::endl;
    out << " + Number of reflected rays per primitive and pixel: "
        << coeff(reflection_rays, n_primitives * n_pixels) << std::endl;
    out << "Rays to calculate refraction: " << refraction_rays << std::endl;
    out << " + Number of refracted rays per primitive and pixel: "
        << coeff(refraction_rays, n_primitives * n_pixels) << std::endl;
    out << "Total reflection and refraction: "
        << reflection_rays + refraction_rays << std::endl;
    out << " + Number of reflected and refracted rays per primitive and pixel: "
        << coeff(refraction_rays + reflection_rays, n_primitives * n_pixels) << std::endl;
    out << std::endl;

    out << "Number of intersection tests: "
        << pos_intersect_tests + neg_intersect_tests
        << std::endl;
    out << " + Intersection tests per primitive: "
        << coeff(pos_intersect_tests + neg_intersect_tests, n_primitives)
        << std::endl;

    out << " + Positive tests: " << pos_intersect_tests << std::endl;
    out << " + Negative: " << neg_intersect_tests << std::endl;

    double ratio = ((double)pos_intersect_tests) /
                   ((double)neg_intersect_tests + pos_intersect_tests);

    out << " + Proportion of positive tests from total: "
        << coeff(pos_intersect_tests, pos_intersect_tests + neg_intersect_tests)
        << std::endl;
}

void Logger::countLastRecursionRay() {last_recursion_rays++;}
void Logger::traceCameraRay() { camera_rays++; }
void Logger::traceShadowRay() { shadow_rays++; }
void Logger::traceRefractionRay() { refraction_rays++; }
void Logger::traceReflectionRay() { reflection_rays++; }
void Logger::countIntersectionTest(bool result)
{
    if(result)
        pos_intersect_tests++;
    else
        neg_intersect_tests++;
}

void Logger::report(const char *filename)
{
    generate_report(std::cout);

    std::ofstream ofs;
    ofs.open(filename);
    generate_report(ofs);
    ofs.close();
}

void Logger::operator+=(Logger other)
{
    last_recursion_rays += other.last_recursion_rays;
    camera_rays += other.camera_rays;
    shadow_rays += other.shadow_rays;
    refraction_rays += other.refraction_rays;
    reflection_rays += other.reflection_rays;
    neg_intersect_tests += other.neg_intersect_tests;
    pos_intersect_tests += other.pos_intersect_tests;
}


void Logger::setNPrimitives(int n) { n_primitives = n; }
void Logger::setNLights(int n) { n_lights = n; }
void Logger::setResolution(int xres, int yres, int antialias)
{
    n_pixels = xres * yres;
    n_antialias = 2;
    for(int i=1; i<=antialias; i++)
        n_antialias *= n_antialias;
}

