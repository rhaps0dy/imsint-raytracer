#include "plane.h"
#include "utils.h"

/*-<==>-----------------------------------------------------------------
/ n.x * x + n.y * y + n.z * z = d
/----------------------------------------------------------------------*/
CPlane::CPlane(const VECTOR &normal, SCALAR distance)
    : norm(normal)
    , dist(distance)
{
    norm.normalize();
}

bool CPlane::hits(const CLine &line, SCALAR &t_hit)
{
    SCALAR t = (dist - norm.dot(line.loc)) / norm.dot(line.dir);
    if(t < EPSILON)
        return false;
    t_hit = t;
    return true;
}

VECTOR CPlane::getNormal(const VECTOR &loc)
{
    return norm;
}
