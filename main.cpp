#include "raytracer.h"
#include "sphere.h"
#include "plane.h"
#include "cilinder.h"
#include "string.h"
#include <iostream>

/*-<==>-----------------------------------------------------------------
/ Defines the scene
/----------------------------------------------------------------------*/
void CRayTracer::load()
{

    // Add the camera looking at the origin
    camera.lookAt(VECTOR(0, 1, -2), VECTOR(0, 0, 0));
    camera.setRenderParameters(1920, 1080, 60);

    // Define some materials
    materials["white"] = new CSolidMaterial(COLOR(.99,.99,.99), .1);
    materials["blue"] = new CSolidMaterial(COLOR(.01,.01,.99), .1);
    materials["black"] = new CSolidMaterial(COLOR(.01,.01,.01), .1);
    materials["yellow"] = new CSolidMaterial(COLOR(.99,.99,.01), .1);
    materials["check1"] = new CCheckerboardMaterial(materials["blue"], materials["white"], 35);
    materials["check2"] = new CCheckerboardMaterial(materials["black"], materials["yellow"], 40);
    materials["water"] = new CSolidMaterial(COLOR(0.01,0.01,0.01), 1, 1, 1.333);

    CRTObject *o;

    o = new CPlane(VECTOR(0, 1, 0), 0.001);
    o->setMaterial(materials["check1"]);
    objects.push_back(o);

    o = new CPlane(VECTOR(0, 0, -1), 0.001);
    o->setMaterial(materials["check2"]);
    objects.push_back(o);

    o = new CSphere(1);
    o->setMaterial(materials["water"]);
    o->setLocation(VECTOR(0, 1.001, -1.001));
    objects.push_back(o);

    o = new CSphere(1);
    o->setMaterial(materials["water"]);
    o->setLocation(VECTOR(-2, 1.001, -1.001));
    objects.push_back(o);

    o = new CSphere(1);
    o->setMaterial(materials["water"]);
    o->setLocation(VECTOR(2, 1.001, -1.001));
    objects.push_back(o);

    // Add a single white light
    CLight *light = new CLight(VECTOR(1, 2, -4), COLOR(1, 1, 1), 0.8);
    lights.push_back(light);
}

bool CRayTracer::loadSnowflake (const char *filename) {
  FILE *f = fopen (filename, "r");
  if (!f)
    return false;

  // Add the camera looking at the origin
  camera.lookAt (VECTOR(2.1, 1.7, 1.3), VECTOR (0,0,0));
  camera.setRenderParameters (1024,1024,45);

  // Define background color
  background_color = COLOR( 0.078, 0.361, 0.753 );

  // Add a two material
  materials["grey"]    = new CSolidMaterial (COLOR (0.5, 0.45, 0.35), 0.5);
  materials["yellowish"]    = new CSolidMaterial (COLOR (0.99, 0.99, 0.8), 0.3);
  materials["check"] = new CCheckerboardMaterial(materials["grey"], materials["yellowish"], .11);
  materials["mirror"] = new CSolidMaterial(COLOR (0.3, 0.3, 0.3), .99, 1, 0);
  materials["water"] = new CSolidMaterial(COLOR (0.3, 0.3, 0.3), .99, 1, 1.333);

  // Add the ground
  CPlane *plane = new CPlane (VECTOR(0,1,0), -0.5);
  plane->setMaterial (materials["check"]);
  objects.push_back (plane);

  // This is a very simply parser!!
  while (!feof(f)) {
    char buf[512];
    fgets (buf, 511, f);
    if (strncmp (buf, "sphere", 6) == 0) {
      char material[64];
      double x,y,z, rad;
      sscanf (buf, "sphere %s %lf %lf %lf %lf\n", material, &rad, &x,&y,&z);
      CSphere *sph = new CSphere(rad);
      sph->setLocation (VECTOR(x,z,y));
      sph->setMaterial (materials["mirror"]);
      objects.push_back (sph);
    }
  }

  // Add 3 white lights
  lights.push_back (new CLight(VECTOR ( 4, 2, 3), COLOR (.3,.3,.3), .01));
  lights.push_back (new CLight(VECTOR ( 1, 4,-4), COLOR (.3,.3,.3), .01));
  lights.push_back (new CLight(VECTOR (-3, 5, 1), COLOR (.3,.3,.3), .01));

  getLogger().setNPrimitives(objects.size());
  getLogger().setNLights(lights.size());
  getLogger().setResolution(camera.getXRes(), camera.getYRes(), ANTIALIAS);

  fclose (f);
  return true;
}


/*-<==>-----------------------------------------------------------------
/ MAIN
/----------------------------------------------------------------------*/
const char *default_render_name = "out.tga";

int main(int argc, char **argv)
{
    CRayTracer rt;
    if(argc <= 1) {
        std::cout << "Usage: "
                  << argv[0]
                  << " [<file_to_render>] [<render_output_file>] <stats_output_file>"
                  << std::endl;
        return 0;
    }

    const char *statsname, *rendername;

    if (argc > 2)
        rt.loadSnowflake(argv[1]);
    else
        rt.load();

    if(argc == 3) {
        rendername = default_render_name;
        statsname = argv[2];
    } else {
        rendername = argv[2];
        statsname = argv[3];
    }
    rt.render(rendername);
    rt.getLogger().report(statsname);
    return 0;
}
