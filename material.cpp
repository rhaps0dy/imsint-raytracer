#include "raytracer.h"
#include <cmath>

#include <iostream>

/*-<==>-----------------------------------------------------------------
/
/----------------------------------------------------------------------*/
CSolidMaterial::CSolidMaterial(const COLOR diff, SCALAR spec, SCALAR trans, SCALAR ior)
    : diffuse_color(diff), specularity(spec), translucency(trans), ind_or(ior)
{
}

COLOR CSolidMaterial::getDiffuseColor(const VECTOR &loc) const
{
    return diffuse_color;
}
SCALAR CSolidMaterial::getSpecularity(const VECTOR &loc) const
{
    return specularity;
}
SCALAR CSolidMaterial::getTranslucency(const VECTOR &loc) const
{
    return translucency;
}
SCALAR CSolidMaterial::getIOR(const VECTOR &loc) const
{
    return ind_or;
}

CCheckerboardMaterial::CCheckerboardMaterial(CMaterial *even, CMaterial *odd, SCALAR grid_size) :
    _even(even), _odd(odd), _grid_size(grid_size)
{
}
const CMaterial *CCheckerboardMaterial::matAtLoc(const VECTOR &loc) const
{
    bool is_even = true;
    for(int i=0; i<3; i++) {
	bool neg = loc[i] > 0;
        bool cur_even = fmod(fabs(loc[i]), _grid_size*2) < _grid_size;
        if((cur_even && !neg) || (!cur_even && neg))
            is_even = !is_even;
    }
    if(is_even)
        return _even;
    return _odd;
}

COLOR CCheckerboardMaterial::getDiffuseColor(const VECTOR &loc) const
{
    return matAtLoc(loc)->getDiffuseColor(loc);
}
SCALAR CCheckerboardMaterial::getSpecularity(const VECTOR &loc) const
{
    return matAtLoc(loc)->getSpecularity(loc);
}
SCALAR CCheckerboardMaterial::getTranslucency(const VECTOR &loc) const
{
    return matAtLoc(loc)->getTranslucency(loc);
}
SCALAR CCheckerboardMaterial::getIOR(const VECTOR &loc) const
{
    return matAtLoc(loc)->getIOR(loc);
}
