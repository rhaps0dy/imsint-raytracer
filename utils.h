#ifndef INCLUDE_LINE_H
#define INCLUDE_LINE_H

#define EPSILON (1e-6)

static SCALAR
min_positive(SCALAR a, SCALAR b)
{
    if(a < b) {
        if(a > 0)
            return a;
        return b;
    }
    if(b > 0)
        return b;
    return a;
}

static SCALAR
clamp(SCALAR x, SCALAR low, SCALAR high)
{
    return x < low ? low : (x > high ? high : x);
}

static COLOR
clamp_color(COLOR c)
{
    c.x = clamp(c.x, 0, 1);
    c.y = clamp(c.y, 0, 1);
    c.z = clamp(c.z, 0, 1);
    return c;
}

#endif
