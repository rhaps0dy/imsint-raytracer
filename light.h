#ifndef INC_LIGHT_H_
#define INC_LIGHT_H_

/*-<==>-----------------------------------------------------------------
/ Basic point light with a single color
/----------------------------------------------------------------------*/
class CLight
{
    VECTOR loc;
    COLOR color;
    SCALAR attenuation;

  public:
    CLight(const VECTOR &aloc, const VECTOR &acolor, const SCALAR att)
        : loc(aloc)
        , color(acolor)
	, attenuation(att)
    {
    }
    COLOR getColor() const
    {
        return color;
    }
    VECTOR getLocation() const
    {
        return loc;
    }
    SCALAR getAttenuation() const
    {
	return attenuation;
    }
};

#include <list>
typedef std::list<CLight *> LLights;

#endif
