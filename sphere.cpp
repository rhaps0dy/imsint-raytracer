#include "sphere.h"
#include "utils.h"

/*-<==>-----------------------------------------------------------------
/ Constructor
/----------------------------------------------------------------------*/
CSphere::CSphere(SCALAR a_radius)
    : radius(a_radius*a_radius)
{
}

/*-<==>-----------------------------------------------------------------
/
/----------------------------------------------------------------------*/
bool CSphere::hits(const CLine &line, SCALAR &t_hit)
{
    VECTOR spos = line.loc - loc;
    SCALAR b = spos.dot(line.dir);
    SCALAR c = spos.dot(spos) - radius;
    SCALAR disc = b * b - c;
    if(disc < 0)
        return false;
    disc = sqrt(disc);
    SCALAR t = -b-disc;
    if(t > EPSILON) {
        t_hit = t;
        return true;
    }
    t = -b+disc;
    if(t > EPSILON) {
        t_hit = t;
        return true;
    }
    return false;
}

VECTOR CSphere::getNormal(const VECTOR &hit_loc)
{
    VECTOR norm = hit_loc - loc;
    norm.normalize();
    return norm;
}
