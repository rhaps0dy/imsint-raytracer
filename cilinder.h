#ifndef INC_CILINDER_H_
#define INC_CILINDER_H_

#include "raytracer.h"

class CCilinder : public CRTObject
{
    SCALAR radius2;
    SCALAR height;

  public:
    CCilinder(SCALAR aradius, SCALAR aheight);
    bool hits(const CLine &line, SCALAR &hits);
    VECTOR getNormal(const VECTOR &loc);
};


#endif
