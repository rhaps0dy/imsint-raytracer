#ifndef INC_RAY_TRACER_H_
#define INC_RAY_TRACER_H_

#include <iostream>
#include "geometry.h"
#include "material.h"
#include "rtobject.h"
#include "camera.h"
#include "light.h"
#include <random>
#include "logger.h"

/*-<==>-----------------------------------------------------------------
/ Raytracer
/---------------------------------------------------------------------*/
class CRayTracer
{
    // Maximum recursion level allowed while computing
    // lines
    int max_recursion_level;

    CCamera camera;       // The camera to render the scene from
    LRTObjects objects;   // List of objects defined in the scene
    MMaterials materials; // Map of materials defined in the scene
    LLights lights;       // List of lights defined in the world

    COLOR background_color;
    COLOR ambient_color;
    std::default_random_engine random_generator;

    void trace(CLine &line, Logger &logger);
    bool intersects(CLine &line, Logger &logger);
    void background(CLine &line);

    Logger logger;

  public:
    CRayTracer();
    void render(const char *filename);
    void load(); // defined in main.cpp
    bool loadSnowflake (const char *filename);
    Logger &getLogger() { return logger; };

    static const int ANTIALIAS = 2;
};

// Send a vector to a output stream
std::ostream &operator<<(std::ostream &os, const VECTOR &v);

#endif
